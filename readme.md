# How to use this Svelte/Tauri Project

This is a tour Svelte project that you can use to build web applications. In order to use this project, you will need to follow a few steps.

## Prerequisites

Before you can use this project, you will need to have the following software installed on your machine:

- Node.js
- NPM

## Installation

1. Install `pnpm` globally by running the following command:

```
npm i -g pnpm
```

2. Clone the repository and navigate to the project directory.

3. Run the following command to install all the required packages:

```
pnpm i
```

## Usage

### Development

To run the project in development mode, use the following command:

```
pnpm tauri dev
```

This will start the development server and open a tauri app with your web application.

### Build

To build your application for production, use the following command:

```
pnpm tauri build
```

This will create a build of your application.

## Additional Information

This project comes with several tools and libraries already installed, including:

- Svelte
- Tauri
- Tailwind
- PostCSS

You can modify the source code and customize the project to fit your specific needs.
